const mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');

const TodohistoricSchema = mongoose.Schema({
    _id : Number,
    description : {
        type: String,
        required: [true, 'description for todos historic required']
      },
    type : {
        type : Number,
        validate: {
            validator: (type) => {
                var result = false;
                if(type == 0 ||  type == 1 || type == 2 ){
                    result = true;
                }
                return result;
            },
            message: props => `${props.value} is not a valid type!`
          },
        required: [true, 'Type required']
    },
    createdAt : {
        type: Date,
        required: [true, 'Date for todos historic required']
      }
}, {
    timestamps: true
});

TodohistoricSchema.index({ createdAt : 1 });
TodohistoricSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('Todohistoric', TodohistoricSchema);


