const mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');

const TodoSchema = mongoose.Schema({
    _id : Number,
    name: {
        type: String,
        required: [true, 'Name for todo required']
      },
    description : String,
    deleted : {
      type: Boolean,
      required: [true, 'Logical Deleted for todo required']
    },
    createdAt : {
        type: Date,
        required: [true, 'Date for todo required']
      }
}, {
    timestamps: true
});

TodoSchema.index({name : 1 , description : -1});
TodoSchema.plugin(mongoosePaginate);


module.exports = mongoose.model('Todo', TodoSchema);


