import CreateTodo from '../../components/CreateTodo/CreateTodo.vue';
import ListTodo from '../../components/ListTodo/ListTodo.vue';
import { apiService } from '../../store/apiServie.js';

export default {
	name: 'index',
	components: {
		CreateTodo,
		ListTodo
	},
	data() {
		return {

		};
	},
	mounted() {
		//Se levanta una coneccion con el soket en este componente
		apiService.state.socket.on('messages', (data) => {
			//console.warn(data);
			this.$swal(data.toUpperCase());
		});
	}
}