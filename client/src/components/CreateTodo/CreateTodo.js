import { apiService } from '../../store/apiServie.js';

export default {
	name: 'CreateTodo',

	components: {},

	data() {
		return {
			showError: false,
			errors : [],
			todo: {
				name : '', createdAt : new Date() , description : '' , deleted : false
			}
		};
	},

	methods: {
		createTodo() {
			this.errors = [];
			if(this.todo.name == ''){
				this.errors.push('Nombre requerido.');
			}
			if(this.todo.createdAt == ''){
				this.errors.push('Fecha requerida.');
			}
			if(this.errors.length == 0){
				apiService.createTodo(this.todo).then(
					()=>{
						//
						var today = new Date();
						var todayFixed = today.getFullYear() + '-' + (today.getMonth()+1) + '-' + today.getDate();
						this.addMessage('Luismi' , 'New todo with name '+ this.todo.name + ' saved ');
						this.todo =  {
							name : '', createdAt : todayFixed , description : '', deleted : false
						};
					}
				).catch();
			}
		},
		updateTodo() {
			apiService.updateTodo(this.todo);
		},
		newTodo() {
			this.todo = {};
		},
		addMessage(author , text) {
			/*var message = { author , text };
			//Se manda el mensage atraves del socket con el evento de new-message
			apiService.state.socket.emit('new-message', message);*/
			apiService.state.socket.emit('new-message',text);
		}
	},

	mounted() {
		//console.log(this.$route.params.id);
		var today = new Date();
		var todayFixed = today.getFullYear() + '-' + (today.getMonth()+1) + '-' + 
			('0' + (today.getDate() + 1)).slice(-2) ;
		this.todo.createdAt = todayFixed;
		//console.log(todayFixed);
		//carga un todo segun la url /todo-update/:id
		/*if (this.$route.params.id) {
			apiService.getTodo(this.$route.params.id).then(todo => {
				this.todo = todo;
				//console.log(this.todo);
			});
		}*/
	}
};