import axios from 'axios';
const API_URL = 'http://localhost:4000';
import io from 'socket.io-client';

export const apiService = {
	state: {
		todoList : [{}],
		numberOfTodos : 0,
		pages : 0,
		actualPage : 1,
		//Se levanta la coneccion con el soket en el store para hacerlo disponible en la app
		socket : io.connect('http://localhost:4000', { 'forceNew': true })
	},
	//
	//APPI CRUD
	getTodosByPage() {
		const url = `${API_URL}/todos-page/${this.state.actualPage}`;
		axios.get(url).then(response => response.data)
			.then((data) => {
				this.state.todoList = data.data.docs;
				this.state.numberOfTodos = data.data.total;
				this.state.pages = data.data.pages;
				if(this.state.pages < this.state.actualPage){
					this.state.actualPage--;
					this.getTodosByPage();
				}else{
					for(let i = 0; i < data.data.docs.length; i++){
						this.state.todoList[i].editing  = false ;
						//Mongo regresa la fecha como un string por eso
						// para manipular como Date se tiene que castear
						this.state.todoList[i].createdAt = new Date(this.state.todoList[i].createdAt);
					}
					//this.state.todoList.splice(data.data.docs.length);
					//console.log(this.state);
				}
			});
	},
	getTodos() {
		return new Promise((resolve ,reject ) =>{
			const url = `${API_URL}/todos/`;
			axios.get(url).then(response => response.data)
				.then((data) => {
					//console.log(data);
					this.state.todoList = data;
					this.state.numberOfTodos = data.length;
					for(let i = 0; i < data.length; i++){
						this.state.todoList[i].editing  = false ;
						//Mongo regresa la fecha como un string por eso
						// para manipular como Date se tiene que castear
						this.state.todoList[i].createdAt = new Date(this.state.todoList[i].createdAt);
					}
					//console.log(this.state.todoList);
					resolve(this.state.todoList.slice(0,10));
				});
		});
	},
	getTodo(todo) {
		const url = `${API_URL}/todos/${todo}`;
		return axios.get(url).then(response => response.data);
	},
	createTodo(todo) {
		return new Promise(
			(resolve,reject)=>{
				const url = `${API_URL}/create/`;
				//console.log(todo);
				axios.post(url, todo).then(
					result => {
						//console.log(result);
						if (result.status === 200) {
							this.todo = result.data;
							alert('Todo added');
							//this.$router.go();
							this.getTodosByPage();
							resolve();
						}
					},
					error => {
						this.showError = true;
						reject();
					}
				);
			}
		);
	},
	updateTodo(todo) {
		const url = `${API_URL}/todos/${todo._id}`;
		axios.put(url, todo).then(
			result => {
				if (result.status === 200) {
					this.todo = result.data;
					alert('Todo updated');
					//this.$router.go();
					this.getTodosByPage();
				}
			},
			error => {
				this.showError = true;
			}
		);
	},
	deleteTodo(todo) {
		const url = `${API_URL}/todos/${todo._id}`;
		axios.delete(url).then(response => {
			if (response.status === 200) {
				alert('Todo deleted');
				//this.$router.push({ path: '/'});
				//this.$router.go();
          
				this.getTodosByPage();
			}
		}).catch( err =>{
			console.error(err)
		});
	},
	enableEdit(index){
		//console.log(this.state.todoList[index]);
		if(!this.state.todoList[index].editing){
			this.state.todoList[index].editing = true;
		}else{
			this.state.todoList[index].editing = false;
		}
		this.state.todoList.splice(this.state.numberOfTodos);
		//console.log(this.state.todoList[index].editing);
	},
	nextPage(tableOpc){
		if(this.state.actualPage < this.state.pages){
			this.state.actualPage++;
			if(tableOpc == 0){
				this.getTodosByPage();
			}else if(tableOpc == 1){
				this.getTodosHistoricByPage(this.state.actualPage,20);
			}
        
		}
	},
	prevPage(tableOpc){
		if(this.state.actualPage > 1){
			this.state.actualPage--;
			if(tableOpc == 0){
				this.getTodosByPage();
			}else if(tableOpc == 1){
				this.getTodosHistoricByPage(this.state.actualPage,20);
			}
		}
	},
	getTodosHistoricByPage(pageNum,type) {
		const url = `${API_URL}/historic-todos-page/${pageNum}/${type}`;
		axios.get(url).then(response => {
			var data = response.data.data;
			this.state.todoList = data.docs;
			this.state.numberOfTodos = data.total;
			this.state.pages = data.pages;
			if(this.state.pages < this.state.actualPage){
				this.state.actualPage--;
				this.getTodosByPage();
			}else{
				for(let i = 0; i < data.docs.length; i++){
					this.state.todoList[i].editing  = false ;
					//Mongo regresa la fecha como un string por eso
					// para manipular como Date se tiene que castear
					this.state.todoList[i].createdAt = new Date(this.state.todoList[i].createdAt);
				}
				this.state.todoList.splice(data.docs.length);
			}
		});
	},
};