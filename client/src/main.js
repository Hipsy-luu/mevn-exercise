import Vue from 'vue';
import BootstrapVue from 'bootstrap-vue';
//Componente o libreria para crear alertas o notificaciones 
import VueSweetalert2 from 'vue-sweetalert2';
// If you don't need the styles, do not connect
import 'sweetalert2/dist/sweetalert2.min.css';


//Importando bootstrap
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';

/*// register jw pagination component globally
import JwPagination from 'jw-vue-pagination';
Vue.component('jw-pagination', JwPagination);*/

import VueRouter from 'vue-router';
//App principal
import App from './App.vue';
//Paginas
import Index from './pages/index/index.vue';
import Historic from './pages/historic/historic.vue';
//Se le agregan los componentes con use
Vue.use(BootstrapVue);
Vue.use(VueSweetalert2);
Vue.use(VueRouter);

const router = new VueRouter({
	mode: 'history',
	routes: [
		{ path: '/', component: Index },
		// dynamic segement `:id` is added to the path
		{ path: '/todo-update/:id', component: Index },
		{ path: '/todo-historic/', component: Historic }
	]
})
Vue.config.productionTip = false;

new Vue({
	router,
	render: h => h(App),
}).$mount('#app')
